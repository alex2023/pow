module blockchain

import time
import crypto.sha256

pub const (
	block_size = int(sizeof(Block))
)

pub struct Block {
pub mut:
	index		  u32
	difficulty 	  u32
	timestamp 	  i64
	hash 		  [32]u8
	previous_hash [32]u8
	nonce 		  u32
}

pub fn Block.genesis() Block {
	return Block{
		difficulty: 2
		timestamp: time.now().unix
	}
}

pub fn Block.new(previous_block Block) Block {
	return Block {
		index: previous_block.index+1
		difficulty: previous_block.difficulty
		timestamp: time.now().unix
		previous_hash: previous_block.hash
	}
}

pub fn (b Block) hex_hash() string {
	return b.hash[..].hex()
}

pub fn (mut b Block) mine() {
	b.previous_hash = b.hash
	for {
		// b.calculate_hash()
		if b.is_valid() {
			break
		}
		b.nonce++
	}
}

pub fn (mut b Block) is_valid() bool {
	b.calculate_hash()
	hex_hash := b.hex_hash()
	prefix_len := int(b.difficulty)/2+1
	return hex_hash[..prefix_len] == '0'.repeat(prefix_len)
}

fn (mut b Block) calculate_hash() {
	prev_hash_hex := b.previous_hash[..].hex()
	// this is very naive, in reality for higher difficullty you may
	// need to try various combinations of data to find a valid hash.
	data := b.index.str()+time.unix(b.timestamp).format()+prev_hash_hex+b.nonce.str()
	for i, b1 in sha256.sum(data.bytes()) {
		b.hash[i] = b1
	}
}
