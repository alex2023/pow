module main

import os
import net
import rand
import config
import blockchain

struct Server {
mut:
	listener   net.TcpListener
	requests   chan &net.TcpConn
	blockchain []blockchain.Block = [blockchain.Block.genesis()]
	wow 	   []string
}


fn Server.new() &Server {
	return &Server{}
}

pub fn (mut s Server) start() {
	s.load_wow(config.wow_filename)
	s.listener = net.listen_tcp(.ip6, '${config.server_addr}:${config.server_port}') or {
		s.error(err)
	}
	s.listen()
}

fn (mut s Server) listen() {
	println('listening...')
	for {
		if mut conn := s.listener.accept() {
			println('received request... sending block to mine...')
			// get prvious block
			previous_block := s.blockchain.last()
			// next block
			mut block := blockchain.Block.new(previous_block)
			// send block to client
			conn.write_ptr(byteptr(&block), blockchain.block_size) or { s.error(err) }
			println('sent block... waiting for solution...')
			// receive solved block
			conn.read_ptr(byteptr(&block), blockchain.block_size) or { s.error(err) }
			println('received solution... validating block...')
			if block.is_valid() {
				block.index++
				s.blockchain << block
				println('valid block... sending valid response (wow/quote)...')
				random_wow := s.random_wow()
				conn.write_string(random_wow) or { s.error(err) }
			} else {
				println('invalid block... sending invalid resoonse...')
				conn.write_string('Invalid Block. Access Denied.') or { s.error(err) }
			}
			conn.close() or { s.error(err) }
		}
	}
}

fn (mut s Server) close() {
	s.listener.close() or { s.error(err) }
}

fn (mut s Server) load_wow(filename string) {
	data := os.read_file(filename) or { s.error(err) }
	s.wow = data.split('\n\n')
}

fn (mut s Server) random_wow() string {
	rand_idx := rand.intn(s.wow.len) or { s.error(err) }
	return s.wow[rand_idx]
}

[noreturn]
fn (mut s Server) error(err IError) {
	println('server error: ${err}')
	exit(1)
}

fn main() {
	mut s := Server.new()
	s.start()
}