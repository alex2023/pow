module config

pub const (
	server_addr  = 'localhost'
	server_port  = 22443
	wow_filename = 'quotes.txt'
)
