module main

import net
import config
import blockchain

struct Client {
mut:
	connection net.TcpConn
}

fn Client.new() &Client {
	return &Client{}
}

fn (mut c Client) connect() {
	c.connection = net.dial_tcp('${config.server_addr}:${config.server_port}') or { c.error(err) }

}

fn (mut c Client) request() {
	mut block := blockchain.Block{}
	// receive block to mine
	c.connection.read_ptr(byteptr(&block), blockchain.block_size) or { c.error(err) }
	// attempt to mine it
	println('mining block...')
	block.mine()
	// try send back block with invalid nonce
	// block.nonce--
	println('mined block (${block.hex_hash()})... sending solution')
	c.connection.write_ptr(byteptr(&block), blockchain.block_size) or { c.error(err) }
	mut response_buf := []u8{len: 4096}
	c.connection.read(mut response_buf) or { c.error(err) }
	println('received response:')
	println(response_buf.bytestr())
}

fn (mut c Client) close() {
	c.connection.close() or { c.error(err) }
}

[noreturn]
fn (mut c Client) error(err IError) {
	println('client error: ${err}')
	exit(1)
}

fn main() {
	mut c := Client.new()
	c.connect()
	c.request()
}