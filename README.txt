Hello,

I decided to write this coding challenge in my language V. It's pretty much Go,
but with immutability, Option/Result, and some other improvements you can read
about here:

https://vlang.io/compare#go

But most importantly it has the performance of C/C++ and an optional GC, which 
is great for blockchain development.

Re-writing this in Go would be trivial.

I wanted to spend no more than 2 hours on this, so I used a simple algorithm.
It's a simplified version of what bitcoin uses. The difficutly level doesn't
increase at the moment, since it was just to be used to prevent DDOS.

It uses sha256 to find a beginning sequence in the sha256 hash.
Based on the difficulty it will try find a sha256 hash with a certain amount
of 0's at the start.

I skipped the Docker part, which is also trivial.

Building V and running this is super easy. V also compiles very fast and only
need a C compiler for bootstrapping.

git clone --depth 1 https://github.com/vlang/v
cd v
make
./v symlink

Or just download it from

https://github.com/vlang/v/releases/latest/download/v_linux.zip

Run the program with
v run client.v
v run server.v

Build optimized binaries with
v -prod client.v && ./client
v -prod server.v && ./server


-Alexander Medvednikov

